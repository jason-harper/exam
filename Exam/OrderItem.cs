﻿using System;

namespace Exam
{
    /// <summary>
    /// Represents a part or service that can be sold as well as the quantity and type of the item ordered.
    /// </summary>
    [Serializable]
    public class OrderItem : Item
    {
        public OrderItem(int key, string name, decimal price, int quantity, bool isService) : base(key, name, price)
        {
            Quantity = quantity;
            IsService = isService;
        }

        public int Quantity { get; }

        public bool IsService { get; }
    }
}
