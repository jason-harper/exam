﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Exam
{
    /// <summary>
    /// Represents an Order that contains a collection of items.
    ///
    /// Care should be taken to ensure that this class is immutable since it
    /// is sent to other systems for processing that should not be able
    /// to change it in any way.
    /// </summary>
    [Serializable]
    public class Order
    {
        private OrderItem[] _orderItems;

        public Order(OrderItem[] orderItems)
        {
            _orderItems = orderItems;
        }

        /// <summary>
        /// Returns the total order cost after the tax has been applied.
        /// </summary>
        public decimal GetOrderTotal(decimal taxRate)
        {
            var total = 0m;

            for(var i = 0; i < _orderItems.Length; i++)
            {
                var orderItem = _orderItems[i];

                if(orderItem.IsService)
                {
                    total += orderItem.Price * orderItem.Quantity;
                }
                else
                {
                    var subTotal = orderItem.Price * orderItem.Quantity;
                    total += subTotal + subTotal * taxRate;
                }
            }

            return Math.Round(total, 2);
        }

        /// <summary>
        /// Returns a List<T> of all items sorted by item name (case-insensitive).
        /// </summary>
        public List<Item> GetItems()
        {
            var items = new Collection<Item>();

            for(var i = 0; i < _orderItems.Length; i++)
            {
                items.Add(_orderItems[i]);
            }

            return items.OrderBy(item => item.Name, StringComparer.OrdinalIgnoreCase).ToList();
        }
    }
}
