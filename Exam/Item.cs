﻿namespace Exam
{
    /// <summary>
    /// Represents a part or service that can be sold.
    ///
    /// Care should be taken to ensure that this class is immutable since it
    /// is sent to other systems for processing that should not be able to
    /// change it in any way.
    /// </summary>
    public class Item
    {
        public Item(int key, string name, decimal price)
        {
            Key = key;
            Name = name;
            Price = price;
        }

        public int Key { get; }

        public string Name { get; }

        public decimal Price { get; }
    }
}
