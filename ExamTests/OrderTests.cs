﻿using Exam;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace ExamTests
{
    [TestClass]
    public class OrderTests
    {
        private OrderItem _orderItem1;
        private OrderItem _orderItem2;
        private OrderItem _orderItem3;

        [TestInitialize]
        public void Init()
        {
            _orderItem1 = new OrderItem(1, "Z Item 1", 1.55m, 2, true);//3.1
            _orderItem2 = new OrderItem(2, "A Item 2", 2.53m, 3, false);//7.59 + .759 = 8.349
            _orderItem3 = new OrderItem(3, "a Item 3", 0, 0, true);
        }

        [TestMethod]
        public void GetOrderTotalShouldReturnCorrectValue()
        {
            OrderItem[] orderItems = { _orderItem1, _orderItem2 };

            var order = new Order(orderItems);

            var total = order.GetOrderTotal(.10m);

            Assert.IsTrue(total == 11.45m);
        }

        [TestMethod]
        public void GetItemsShouldReturnSortedItems()
        {
            var expected = new List<Item>(3)
            {
                _orderItem2,
                _orderItem3,
                _orderItem1
            };

            OrderItem[] orderItems = { _orderItem1, _orderItem2, _orderItem3 };

            var order = new Order(orderItems);

            var actual = order.GetItems();

            CollectionAssert.AreEqual(expected, actual);
        }
    }
}
