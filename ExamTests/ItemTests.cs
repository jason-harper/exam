﻿using Exam;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections;

namespace ExamTests
{
    [TestClass]
    public class ItemTests
    {
        [TestMethod]
        public void ItemShouldBeAbleToBeUsedAsHashtableKey()
        {
            var item1 = new Item(0, string.Empty, 0);
            var item2 = new Item(0, string.Empty, 0);

            Hashtable hashTable = new Hashtable
            {
                { item1, "item1" },
                { item2, "item2" }
            };

            Assert.AreEqual("item1", hashTable[item1]);
            Assert.AreNotEqual("item1", hashTable[item2]);
        }
    }
}
